%% Lab exercise on Geomagnetism and Waves

%% Step 5
% Access MATLAB workspace with all data. 

load('GEOMAG.mat');

%% Step 5.1
% (a) Plot the Dst index as a function of time.
%
% Variables:
% -----------------
%  Dst_time
%  Dst
%
% Functions:
% -----------------
% plotTimeSeries(X1, Y1)

figure(1)
plotTimeSeries();

% (b) Visually detect all moderate and intense geomagnetic storms.
%
% (c) For every geomagnetic storm, find the minimal Dst value, 
% time of peak activity and duration. For this purpose, click on the point
% on the plot to find the corresponding (t, Dst) value.
%
% (d) Find the largest disturbance and zoom on this to create a new plot
% of the most intense storm.
% 

%% Step 5.2
% (a) Plot solar wind parameters: Bz, Vsw and Psw, along with Dst.
% 
% Variables:
% -----------------
%  Dst_time
%  Dst
%  Bz_time
%  Bz
%  Vsw_time
%  Vsw
%  Psw_time
%  Psw
%
% Functions:
% -----------------
% plotTimeSeries(X1, Y1, X2, Y2, X3, Y3, X4, Y4)

figure(2)
plotTimeSeries();

% (b) Identify the time of arrival of the interplanetary shock and the sudden storm
% commencement for the most intense storm.
%
% (c) Find and examine a second case, in which even though there is a CME impacting Earth
% no storm was observed and explain why. You can zoom in on the plot to find details of this second case).

%% Step 5.3
% (a) Plot the magnetometer data from the ENIGMA station Dionisos and 
% compare these with the Dst
% 
% Variables:
% -----------------
%  Dst_time
%  Dst
%  Bgr_time
%  Bgr
%
% Functions:
% -----------------
% plotTimeSeries(X1, Y1, X2, Y2)

figure(3)
plotTimeSeries();

%% (b) Apply a high pass filter on the magnetometer data where the cutoff frequency (in Hz) is below 1 mHz
% to eliminate fluctuations below the Pc5 frequency range. Then, plot the new filtered time series along 
% with the original one.
% 
%
% Variables:
% -----------------
%  Bgr_time
%  Bgr
%
% Functions:
% -----------------
% Yfilt = filterTimeSeries(Y, cutoff_freq)
% plotTimeSeries(X1, Y1, X2, Y2)

figure(4)
BfiltPc5 = filterTimeSeries(Bgr, 1/1000);
plotTimeSeries(Bgr_time, Bgr, Bgr_time, BfiltPc5);

%%
% Repeat the same step, using a cutoff frequency (in Hz) that is below 7 mHz to
% eliminate fluctuations below the Pc4 frequency range. Then plot the new
% filtered time series along with the original one.

figure(5)
BfiltPc4 = filterTimeSeries();
plotTimeSeries();


%%
% (c) Select a time interval of a few hours during the main phase of the storm.
% Apply Fast Fourier Transform (fft) on the Pc5 filtered data that you 
% derived in the previous step, and plot the Fourier Power calculated with respect to the frequency. 
% 
% NOTE: Beware that the maximum frequency does not exceed the Nyquist
% frequency of the time series (sampling time of Bgr is 1 sec)!
%
% Variables:
% -----------------
%  Bgr_time
%  BfiltPc5
%  BfiltPc4
%
% Functions:
% -----------------
% [Freq, FftPower] = fourierSpectrum(Y_time, Y, begin_time, end_time, ...
%    low_freq, high_freq)

begin_time = datenum(2013, 3, 17, 0, 0, 0);
end_time = datenum(2013, 3, 17, 4, 0, 0);
low_freqPc5 = 1/1000;
high_freqPc5 = 7/1000;

[FreqPc5, FftPowerPc5] = fourierSpectrum(Bgr_time, BfiltPc5, begin_time, end_time, low_freqPc5, high_freqPc5);

figure(6)
loglog(FreqPc5, FftPowerPc5);
xlim([low_freqPc5, high_freqPc5]);
xlabel('Freq (Hz)')
ylabel('Fourier Power (nT^2)');

%% Repeat the same step applying fft on the Pc4 filtered data that you 
% derived in the previous step, and plot the Fourier Power calculated with
% respect to the frequency.

low_freqPc4 = ;
high_freqPc4 = ;

[FreqPc4, FftPowerPc4] = fourierSpectrum();

figure(7)
loglog();
xlim([]);
xlabel('Freq (Hz)')
ylabel('Fourier Power (nT^2)');


%%
% (d) Calculate the average of the Fourier Power for every frequency range
% in the specific time interval selected.

% Variables:
% -----------------
%  FftPowerPc5
%  FftPowerPc4
%
% Functions:
% -----------------
% mean(F)

% mean Fourier power in the Pc5 frequency range
avgPc5 = ;

% mean Fourier power in the Pc4 frequency range
avgPc4 = ;

 
%% Step 5.4
% (a) Follow the previous step for every hour in the original timeseries.
% 
% Isolate the storm (set the "begin_time" and "end_time" parameters)
% and use the function fourierPower to compute the mean Fourier power
% within this time period, for the Pc5 frequency range. Use the filtered data 
% you have already calculated in step 5.3(b). To define the
% frequency range, use the "low_freq" and "high_freq" parameters. 
% Fourier transform will be applied to a segment of the given time series with
% size equal to "window_size" in seconds. You can set the value of that variable to
% 1 hour. Lastly, plot the mean Fourier power along with the Dst index as a 
% function of time
%
% NOTE: Make sure the "window_size" is large enough for the "freq_low"
% that you have specified!
% 
% Variables:
% -----------------
%  Bgr_time
%  BfiltPc5
%  BfiltPc4
%  Dst_time
%  Dst
%
% Functions:
% -----------------
% [power_time, power] = fourierPower(Y_time, Y, begin_time, end_time, ...
%    window_size, begin_freq, end_freq);

begin_time = datenum(2013, 3, 1, 0, 0, 0);
end_time = datenum(2013, 4, 30, 23, 59, 59);
low_freqPc5 = 2/1000;
high_freqPc5 = 7/1000;
window_size = 3600;

[powerPc5_time, powerPc5] = fourierPower(Bgr_time, BfiltPc5, begin_time, end_time, window_size, low_freqPc5, high_freqPc5);

figure(8)
plotTimeSeries(Dst_time, Dst, powerPc5_time, powerPc5);

%% Repeat the previous step to calculate the mean Fourier power for the same storm
%  over the Pc4 frequence range and plot the results together with Dst as a
%  function of time

low_freqPc4 = ;
high_freqPc4 = ;
window_size = ;

[powerPc4_time, powerPc4] = fourierPower();

figure(9)
plotTimeSeries();



 