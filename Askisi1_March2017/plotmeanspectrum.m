function  meanflux=plotmeanspectrum(T, E, I, time_range)
%fitPowerLaw Performs a power-law type fit on the input data
%
%   e = fitPowerLaw(X, Y)
%   Fits the input data, specified by the X and Y variables, according to a
%   power law fit of the form Y = e1 * X^(-e2). Output argument 'e' is a
%   vector of two elements, with e(1) = e1 and e(2) = e2.
%
%   e = fitPowerLaw(X, Y, init_vals)
%   As above, with an additional parameter of "init_vals", which should be
%   a vector of two elements, specifying the initial values for the fit
%   coefficients e(1) and e(2). If it is not given, the default parameters
%   of 5*10^9 and 3.262153178920480482 will be used.
%

% if nargin < 5
%     energy_channels = [1 10];
% end
% 
% if nargin < 4
%     time_range = [T(1) T(end)];
% end


% Figure Parameters
LW=2;
MSz=18;
FSz=12;
FWt='demi';
FName='Arial';
%--------------------------------------------------------------------------
POS=get(gcf,'Position');delete(gcf);
    figure('position',POS,'color','w',.....
   'defaultlinelinewidth',LW,... 
   'DefaultAxeslinewidth',LW,... 
   'DefaultTextFontName', 'FName',...
   'DefaultTextFontSize',FSz, ... 
   'DefaultAxesFontName', 'FName',...
   'DefaultAxesFontSize',FSz, ...  
   'DefaultLineMarkerSize', MSz,...
   'DefaultTextFontWeight',FWt,...
   'DefaultAxesFontWeight',FWt);
%--------------------------------------------------------------------------
ind=find(T>time_range(1) & T<time_range(2));

meanflux = mean(I(ind,:), 1)';


plot(E,meanflux,'.k');
set(gca,'xscale','log','yscale','log'); grid on;hold on;
xlabel ('E (keV)'); 
ylim([100 1000000]);
xlim([10 5000]);
ylabel ('I (cm^{2}\cdotst\cdotsec\cdotkeV)^{-1}','fontsize',14,'fontweight','b');
end
