function plotTimeSeries0(varargin)

% check last input argument to see if the user declared x limits
s = size(varargin{end});
if all(s == [2,1]) || all(s == [1,2])
    xlims = varargin{end};
else
    xlims = [];
end
    

% if no 'time' variable was given, assume that the data correspond to a
% sinlge day and create a dummy time index
if nargin == 1
    varargin{2} = varargin{1};
    varargin{1} = linspace(1,2, length(varargin{2}));
end

nPlots = fix(length(varargin)/2);
ax = zeros(1, nPlots);

% figure;

for i=1:nPlots
    X = varargin{2*(i-1) + 1};
    Y = varargin{2*(i-1) + 2};
    
    Ymin = min(Y);
    Ymax = max(Y);
    dY = 1.2*(Ymax - Ymin);
    if dY == 0; dY = 1; end
    
    ax(i) = subplot(nPlots, 1, i);
    plot(X, Y, '-b', [X(1), X(end)], [0, 0], '-k');
    if ~isempty(xlims)
        set(ax(i), 'xlim', [xlims(1), xlims(end)], 'ylim', [Ymin-0.1*dY, Ymax+0.1*dY]);
    else
        set(ax(i), 'xlim', [X(1), X(end)], 'ylim', [Ymin-0.1*dY, Ymax+0.1*dY]);
    end
    ylabel(inputname(2*(i-1) + 2));
    set(ax(i), 'XTickMode', 'auto');
    datetick(ax(i), 'keeplimits');
end

end
