function fY = filterTimeSeries(Y, cutoff, SAMPLING_TIME)
%filterTimeSeries 

if nargin < 3
    SAMPLING_TIME = 1;
end

% order = 5;
% type = 'high';
% [B, A] = butter(order, cutoff/((1/SAMPLING_TIME)/2), type);
% fY = filtfilt(B, A, Y);
% 
% fY1 = filter(B, A, Y);
% fY2 = filter(B, A, fY1(end:-1:1));
% fY0 = fY2(end:-1:1);

period = ceil(.45/(cutoff*SAMPLING_TIME));
fM1 = eqn_movingAverageNaN(Y, period);
fM2 = eqn_movingAverageNaN(fM1(end:-1:1), period);
fM0 = Y - fM2(end:-1:1);

%plot(1:length(fY), fY, '-ob', 1:length(fY0), fY0, '-xr', 1:length(fM0), fM0, '-+g');

fY = fM0;

end

