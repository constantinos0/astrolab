function plotTimeSeries(varargin)

% if no 'time' variable was given, assume that the data correspond to a
% single day and create a dummy time index
if nargin == 1
    varargin{2} = varargin{1};
    varargin{1} = linspace(1,2, length(varargin{2}));
end

if mod(nargin, 2)~=0
    %error('plotTimeSeries: Requires an even number of input arguments. In example, use something like plotTimeSeries(X, Y) or plotTimeSeries(X1, Y1, X2, Y2) or plotTimeSeries(X1, Y1, X2, Y2, X3, Y3) etc.')
    units = varargin{end};
else
    units = cell(nargin/2, 1);
end

nPlots = fix(length(varargin)/2);
% global ax;
ax = zeros(1, nPlots);

% figure;

for i=1:nPlots
    X = varargin{2*(i-1) + 1};
    Y = varargin{2*(i-1) + 2};
    
    Ymin = min(Y);
    Ymax = max(Y);
    dY = 1.2*(Ymax - Ymin);
    if dY == 0; dY = 1; end
    
    ax(i) = subplot(nPlots, 1, i);
    plot(X, Y, '-b', [X(1), X(end)], [0, 0], '-k');
    set(ax(i), 'xlim', [X(1), X(end)], 'ylim', [Ymin-0.1*dY, Ymax+0.1*dY]);
    ylabel([inputname(2*(i-1) + 2), ' ', units{i}]);
end

zh = zoom;
set(zh, 'ActionPostCallback', @(obj, evd, ax_ptr)mypostcallback(ax));
set(zh, 'Enable', 'on');
ph = pan;
set(ph, 'ActionPostCallback', @(obj, evd, ax_ptr)mypostcallback(ax));
set(ph, 'Enable', 'on');

linkaxes(ax, 'x');
mypostcallback(ax);

end

function mypostcallback(ax_ptr)
    for i=1:length(ax_ptr)
        set(ax_ptr(i), 'XTickMode', 'auto');
        datetick(ax_ptr(i), 'keeplimits');
    end
end
