%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% THIS PROGRAM LOADS RBSP PARAMETERS AND CALCULATES THE ELECTRON SPECTRUM,
% AND THE POWER LAW FIT TO THIS SPECTRUM 
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
clear all;close all;clc

%--------------------------------------------------------------------------
% LOAD DATA FILES
%--------------------------------------------------------------------------

load 'RBSP.mat';
load 'RBSPenergy.mat';

%% Calculate �� = Emax-Emin

DE=Emax-Emin; 
 
%% Create Differential Flux Matrix for RBSP using the equation I = C/(DE*G*Dt)

for i=1:10
I(:,i)=CountRate(:,i)./(DE(i).*G(i));
end

%--------------------------------------------------------------------------

%% Plot Differential Flux Profiles Using the Function: plotfluxes(T, MLT, Lstar, I, [initial time, final time])

figure(1);
plotfluxes();

%% Plot Differential Flux Profiles for the first day Using the Function: plotfluxes(T, MLT, Lstar, I, [initial time, final time])
figure(2);
plotfluxes();

%%
%--------------------------------------------------------------------------
% Create pre-storm spectrum using the Function: plotmeanspectrum(T,E,I,[initial time, final time])
%--------------------------------------------------------------------------

figure(3);
plotmeanspectrum();


%% Low-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);
      
e = fitPowerLaw();
prestorm_parameters1 = e;  

%% High-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);


e = fitPowerLaw();
prestorm_parameters2 = e;     
  
%%
%--------------------------------------------------------------------------
% Create main--phase spectrum using the Function: plotmeanspectrum(T,E,I,[initial time, final time])
%--------------------------------------------------------------------------

figure(4);
plotmeanspectrum();


%% Low-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);
      
e = fitPowerLaw();
main_parameters1 = e;  

%% High-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);

e = fitPowerLaw();
main_parameters2 = e;     

%%
%--------------------------------------------------------------------------
% Create recovery--phase spectrum using the Function: plotmeanspectrum(T,E,I,[initial time, final time])
%--------------------------------------------------------------------------

figure(5);
plotmeanspectrum();


%% Low-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);
      
e = fitPowerLaw();
recovery_parameters1 = e;  

%% High-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);

e = fitPowerLaw();
recovery_parameters2 = e;     

%%
%--------------------------------------------------------------------------
% Create post-storm spectrum using the Function: plotmeanspectrum(T,E,I,[initial time, final time])
%--------------------------------------------------------------------------

figure(6);
plotmeanspectrum();


%% Low-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);
      
e = fitPowerLaw();
poststorm_parameters1 = e;  

%% High-energy Fitting Process using the Function: e = fitPowerLaw(T, E, I, [initial time, final time],[initial channel, final channel]);

e = fitPowerLaw();
poststorm_parameters2 = e;     

%--------------------------------------------------------------------------
% END OF CODE
%--------------------------------------------------------------------------