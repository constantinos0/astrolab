function  plotindices(T, DST, AL, range)
%fitPowerLaw Performs a power-law type fit on the input data
%
%   e = fitPowerLaw(X, Y)
%   Fits the input data, specified by the X and Y variables, according to a
%   power law fit of the form Y = e1 * X^(-e2). Output argument 'e' is a
%   vector of two elements, with e(1) = e1 and e(2) = e2.
%
%   e = fitPowerLaw(X, Y, init_vals)
%   As above, with an additional parameter of "init_vals", which should be
%   a vector of two elements, specifying the initial values for the fit
%   coefficients e(1) and e(2). If it is not given, the default parameters
%   of 5*10^9 and 3.262153178920480482 will be used.
%

if nargin < 4
    range = [T(1) T(end)];
end

if nargin < 4
    date_format = 'dd/mm';
else
    date_format = 'dd/mm, HH:MM';
end

% Figure Parameters
LW=2;
MSz=18;
FSz=12;
FWt='demi';
FName='Arial';
%--------------------------------------------------------------------------
POS=get(gcf,'Position');delete(gcf);
    figure('position',POS,'color','w',.....
   'defaultlinelinewidth',LW,... 
   'DefaultAxeslinewidth',LW,... 
   'DefaultTextFontName', 'FName',...
   'DefaultTextFontSize',FSz, ... 
   'DefaultAxesFontName', 'FName',...
   'DefaultAxesFontSize',FSz, ...  
   'DefaultLineMarkerSize', MSz,...
   'DefaultTextFontWeight',FWt,...
   'DefaultAxesFontWeight',FWt);
%--------------------------------------------------------------------------
ind=find(T>range(1) & T<range(2));


subplot(2,1,1)
plot(T(ind), DST(ind));
set(gca,'xscale','lin','yscale','lin'); grid on;
datetick('x',date_format);
xlabel ('Time'); 
% ylim([1 10000000]);
ylabel ('Dst Index (nT)','fontsize',14,'fontweight','b');
% h=legend('Channel 1 [37.3 keV]','Channel 5 [184.9 keV]','Channel 9 [1535 keV]',0);
title('Geomagnetic Indices','fontsize',14,'fontweight','b');

subplot(2,1,2)
plot (T(ind), AL(ind) );
set(gca,'xscale','lin','yscale','lin'); grid on;
xlabel ('Time'); 
datetick('x',date_format);
% ylim([1 6]);
ylabel ('AL Index (nT)','fontsize',14,'fontweight','b');
% title('Magnetic Shell');

end
